(import '(java.util Scanner))

(println "Introduce las horas trabajadas ")
(def scan (Scanner. *in*))
(def horas_trabajadas(.nextInt scan))

(def salario_minimo 1)

(def horas_extras(- horas_trabajadas 40))

(def tarifa_extra (+ salario_minimo (* salario_minimo 0.5)))

(def tarifa1(* horas_extras tarifa_extra))
(def tarifa2(* 40 salario_minimo))

(def sueldo_sin (* horas_trabajadas salario_minimo) )
(def sueldo_con (+ tarifa1 tarifa2))


(if (<= horas_trabajadas 40) 
	(def salario sueldo_sin ))

(if (> horas_trabajadas 40) 
	(def salario sueldo_con))

(println "Tu salario es:"  salario)


