(import '(java.util Scanner))
(println  "=> Introduce un numero:")
(def scan (Scanner. *in*))
(def n(.nextDouble scan))
(if (> (mod n 1) 0)(println "El numero" n "Si tiene Parte Fraccionaria" "   Parte Entera" (quot n 1) "  Parte Decimal=" (rem n 1)) (println  "El numero" n "No tiene Parte Fraccionaria")
)
