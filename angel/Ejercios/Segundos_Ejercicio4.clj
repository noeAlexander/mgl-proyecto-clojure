(import '(java.util Scanner))
(println  "=> Introduce Tiempo en segundos:")
(def scan (Scanner. *in*))
(def  tiempo_seg(.nextInt scan))


(if(>=   (mod tiempo_seg 60) 0)
(do
	(def minutos(quot tiempo_seg 60));(+ (quot tiempo_seg 60) 1))
	(def segundos(mod tiempo_seg 60))

	(print minutos "min  :" segundos "seg. " " faltan" (+ (* segundos -1) 60) "seg. para" (+ minutos 1) "min.")
	) 
) 

