(import '(java.util Scanner))
(def scan (Scanner. *in*))
(println "Introduce tiempo en segundos: ")
(def tiempoS (.nextFloat scan))

(if(> (mod tiempoS 60) 0) 
(println "Faltan:"(+ (* (mod tiempoS 60) -1) 60) " Segundos para los " (+ (quot tiempoS 60) 1) "Minutos") 

)

(if(== (mod tiempoS 60) 0) 
	(println "Falta 1 minuto para los "(+ (quot tiempoS 60) 1) "minutos.")

)