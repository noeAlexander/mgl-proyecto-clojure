(import '(java.util Scanner))
(def scan (Scanner. *in*))
(println "Algoritmo recursivo que permite sumar los elementos de una matriz.")
(println "Dime numero de filas:")
(def fila (.nextInt scan))
(println "Dime numero de columnas:")
(def col (.nextInt scan))

(def matriz  [[5 2 3][5 2 3]])
(def orden 2)
(def suma (atom 0))


(defn sumar-posiciones-matriz [fila col orden m]
(println "Fila:" fila "columnas" col )	
(if(and (= fila 0) (= col 0))
(;;<------CASO RECURSIVO
	(println "")
(swap! suma + (get-in m [fila col]))
(println "La suma de los elementos de la matriz es:" @suma)

(System/exit 0);; Salir del programa
);;si es verdadero

(if (< col 0); si es falso ó else <------CASO RECURSIVO
(;; si es verdadero del segundo if
str (sumar-posiciones-matriz (- fila 1) orden orden m)

)
(;; si es falso del segundo if  <------CASO RECURSIVO
	(println @suma "mas" (get-in m [fila col]) )
	(swap! suma + (get-in m [fila col]))
	(sumar-posiciones-matriz fila (- col 1)  orden m)

	)

 
);;cerramos el if del if	
);;cerramos el if principal
);;cerramos la funcion

(sumar-posiciones-matriz fila col orden matriz)