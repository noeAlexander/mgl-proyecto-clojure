(import (java.util Scanner))
(def  teclado (Scanner. *in*))
(println "")
(println "*********************************************")
(println "******    <Calculador de salarios    *******|")
(println "*********************************************")
(println "")
(println "=> Ingrese sus horas trabajas:")
(def horas_trabajadas (.nextDouble teclado))
(def tarifa 1)
(if  (<= horas_trabajadas 40.0) (def salario (* tarifa horas_trabajadas)))
(if (> horas_trabajadas 40) (do (def tarifa_extra (+ tarifa (* 0.5 tarifa)))
	(def horas_extras (- horas_trabajadas 40))(def salario (+ (* horas_extras tarifa_extra) (* 40 tarifa)))))
(println "=> El salario es" salario)
(println "")
(println "*********************************************")
(println "************      ->  FIN  <-    ***********|")
(println "*********************************************")
(println "")
