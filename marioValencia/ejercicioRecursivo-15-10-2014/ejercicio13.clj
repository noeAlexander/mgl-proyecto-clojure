; created by Mario Alberto Valencia Moreno
(println "")
(println "Programa Recursivo")
(println "Ejercicio 13: Cuadro Latino")
(println "")

(def matriz [[0 0 0 0 0][0 0 0 0 0] [0 0 0 0 0][0 0 0 0 0][0 0 0 0 0]])

(defn latino [ fila col cont orden mat]

	(if (and (= fila 0)(= col 0)) (do  ;caso base <-
		(def matAux (assoc (nth matriz 0) 0 1)) ; <-
		(def matriz (assoc matriz 0 matAux))) ; <- lineasa caso base, cuando col y fila sean == 0 matriz[0][0]=0

		(do 

			(if (= fila col) (do 
				(latino (- fila 1) (- orden 1) cont orden mat)) ; Caso recursivo
					(do (def matAux (assoc (nth matriz fila) col cont))
					(def matriz (assoc matriz fila matAux))
					(latino fila (- col 1) (+ cont 1) orden mat)) ;Caso recursivo
	        )
	    )
    
    ) 

)

(latino (count matriz) (count (nth matriz 0)) 0 5 matriz)

(def incremento 0)

(while (< incremento 5)
	(println (nth matriz incremento))
	(def incremento (inc incremento))
)

(println "")
