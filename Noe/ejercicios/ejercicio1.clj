(import '(java.util Scanner))
(println  "===Nomina===")
(def scan (Scanner. *in*))
(println  "¿Cual es la tarifa?")
(def tarifa(.nextInt scan))
(println  "¿Cuales fueron las horas trabajadas?")
(def hTrabajadas(.nextInt scan))
(def salario 0)
(def tarifa_extra 0)
(def horas_extras 0)
;calculo de las horas extra
(def tarifa_extra (+ tarifa (* 0.50 tarifa)))
(def horas_extras (- hTrabajadas 40))
(def operacion (* 40 tarifa))
(def operacion (+ tarifa_extra operacion))
(def operacion (* horas_extras operacion))               
;/////////////////////////////////////////

(if (<= hTrabajadas 40)
    (def salario (* hTrabajadas tarifa) )
    
    (if (> hTrabajadas 40)  
    	(def salario operacion)
    )
)

(println salario)
