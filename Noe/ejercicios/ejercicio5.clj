(import '(java.util Scanner))
(def scan (Scanner. *in*))
(println  "¿Cuales son los minutos?")
(def minutosR(.nextInt scan))
(def dias 0)
(def minutos)
(def horas)
(def x)
(if (>= minutosR 1440)
	(do
		(def dias (quot minutosR 1440))
		(def x (mod minutosR 1440))
		(def horas (quot x 60))
		(def minutos (mod x 60))
	)

	(if (< minutosR 1440)
		(do
			(def dias 0)
			(def horas(quot minutosR 60))
			(def x (mod minutosR 1440))
			(def minutos(mod x 60))

		)

	)

)
(println "dias:"dias " " "horas:" horas " " "minutos:" minutos)

