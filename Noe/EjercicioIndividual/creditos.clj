(import '(java.util Scanner))
(def scan (Scanner. *in*))
(def total_credito 0)
(def numero_veces 0)


(while (<= total_credito 25)
	(do
		(println  "¿Cuantos creditos?")
		(def credito(.nextInt scan))
		(def total_credito (+ total_credito credito))					
		(def numero_veces (+ numero_veces 1))
	)
)


(def a (- total_credito 25))
(def total_credito (- total_credito a))

	
(println  "numero de creditos:" " "total_credito" " "numero de veces insertadas:" numero_veces)
	