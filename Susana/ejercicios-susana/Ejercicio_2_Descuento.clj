(import '(java.util Scanner))
(def teclado (Scanner. *in*))
(def descuento 0)
(def sueldoNeto 0)
(println "---DESCUENTOS---")
(println "Introduce el sueldo")
(def sueldo (.nextInt teclado))

(if (<= sueldo 1000)
	(do 
		(def descuento (* sueldo 0.1))
		(def sueldoNeto (- sueldo descuento)))

	(if (and (> sueldo 1000) (<= sueldo 2000))
		(do
			(def resta (- sueldo 1000))
			(def mult (* resta 0.05))
			(def mult2 (* 1000 0.1))
			(def descuento (+ mult mult2))
			(def sueldoNeto (- sueldo descuento)))

		(if (> sueldo 2000)
			(do 
			(def resta (- sueldo 1000))
			(def mult (* resta 0.03))
			(def mult2 (* 1000 0.1))
			(def descuento (+ mult mult2))
			(def sueldoNeto (- sueldo descuento)))
				)
			)
		)

(println "El duescuento es:")
(println descuento)
(println "El sueldo Neto es:")
(println sueldoNeto)

